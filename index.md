# deanturpin/dev

Full C++ dev environment in a Docker container.

See [Docker](https://hub.docker.com/r/deanturpin/dev).

